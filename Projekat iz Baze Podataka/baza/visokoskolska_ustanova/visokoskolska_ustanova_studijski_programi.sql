-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: visokoskolska_ustanova
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `studijski_programi`
--

DROP TABLE IF EXISTS `studijski_programi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `studijski_programi` (
  `oznaka_programa` varchar(3) NOT NULL,
  `ustanova` char(2) NOT NULL,
  `nivo` decimal(2,0) NOT NULL DEFAULT '0',
  `naziv_programa` varchar(120) NOT NULL DEFAULT '0',
  PRIMARY KEY (`oznaka_programa`),
  KEY `studijski_programi_fk1_idx` (`ustanova`),
  KEY `studijski_programi_fk2_idx` (`nivo`),
  CONSTRAINT `studijski_programi_fk1` FOREIGN KEY (`ustanova`) REFERENCES `visokoskolska_ustanova` (`oznaka`),
  CONSTRAINT `studijski_programi_fk2` FOREIGN KEY (`nivo`) REFERENCES `nivo_studija` (`oznaka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studijski_programi`
--

LOCK TABLES `studijski_programi` WRITE;
/*!40000 ALTER TABLE `studijski_programi` DISABLE KEYS */;
INSERT INTO `studijski_programi` VALUES ('EK','zz',2,'asd'),('IT','us',1,'Informacione tehnologije'),('SII','us',1,'asdasd');
/*!40000 ALTER TABLE `studijski_programi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-30  3:27:12
