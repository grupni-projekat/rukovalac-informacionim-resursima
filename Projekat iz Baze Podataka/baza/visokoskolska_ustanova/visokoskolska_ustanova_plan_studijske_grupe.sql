-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: visokoskolska_ustanova
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `plan_studijske_grupe`
--

DROP TABLE IF EXISTS `plan_studijske_grupe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plan_studijske_grupe` (
  `oznaka_programa` varchar(3) NOT NULL,
  `program_ustanove` char(2) NOT NULL,
  `blok` decimal(2,0) NOT NULL,
  `pozicija` decimal(2,0) NOT NULL,
  `ustanova_predmet` char(2) NOT NULL,
  `oznaka_predmeta` varchar(6) NOT NULL,
  PRIMARY KEY (`oznaka_programa`),
  KEY `plan_studijske_grupe_fk1_idx` (`oznaka_predmeta`),
  KEY `plan_studijske_grupe_fk3_idx` (`program_ustanove`),
  CONSTRAINT `plan_studijske_grupe_fk1` FOREIGN KEY (`oznaka_predmeta`) REFERENCES `nastavni_predmet` (`oznaka`),
  CONSTRAINT `plan_studijske_grupe_fk2` FOREIGN KEY (`oznaka_programa`) REFERENCES `studijski_programi` (`oznaka_programa`),
  CONSTRAINT `plan_studijske_grupe_fk3` FOREIGN KEY (`program_ustanove`) REFERENCES `studijski_programi` (`ustanova`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan_studijske_grupe`
--

LOCK TABLES `plan_studijske_grupe` WRITE;
/*!40000 ALTER TABLE `plan_studijske_grupe` DISABLE KEYS */;
INSERT INTO `plan_studijske_grupe` VALUES ('SII','us',6,3,'as','SIMS');
/*!40000 ALTER TABLE `plan_studijske_grupe` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-30  3:27:13
