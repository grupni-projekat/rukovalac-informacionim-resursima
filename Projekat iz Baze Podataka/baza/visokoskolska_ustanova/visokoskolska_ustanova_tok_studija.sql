-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: visokoskolska_ustanova
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tok_studija`
--

DROP TABLE IF EXISTS `tok_studija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tok_studija` (
  `broj_indeksa` varchar(6) NOT NULL,
  `ustanova` char(2) NOT NULL,
  `oznaka_programa` varchar(3) NOT NULL,
  `student_iz_ustanove` char(2) NOT NULL,
  `struka` char(2) NOT NULL,
  `skolska_godina` decimal(4,0) NOT NULL,
  `godina_studija` decimal(1,0) NOT NULL,
  `blok` decimal(2,0) NOT NULL,
  `redni_broj_upisa` decimal(2,0) NOT NULL,
  `datum_upisa` date NOT NULL,
  `datum_overe` date NOT NULL,
  `espb_pocetni` decimal(3,0) NOT NULL,
  `espb_krajnji` decimal(3,0) NOT NULL,
  PRIMARY KEY (`broj_indeksa`),
  KEY `tok_studija_fk1_idx` (`oznaka_programa`),
  CONSTRAINT `tok_studija_fk1` FOREIGN KEY (`oznaka_programa`) REFERENCES `studijski_programi` (`oznaka_programa`),
  CONSTRAINT `tok_studija_fk2` FOREIGN KEY (`broj_indeksa`) REFERENCES `studenti` (`broj_indeksa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tok_studija`
--

LOCK TABLES `tok_studija` WRITE;
/*!40000 ALTER TABLE `tok_studija` DISABLE KEYS */;
INSERT INTO `tok_studija` VALUES ('123456','zz','SII','us','st',2018,1,2,3,'2020-01-01','2020-01-05',0,120),('270465','as','SII','us','st',2020,1,2,3,'2018-01-01','2019-01-01',0,240);
/*!40000 ALTER TABLE `tok_studija` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-30  3:27:12
