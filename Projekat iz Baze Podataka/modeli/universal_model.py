from PySide2 import QtCore

class UniversalModel(QtCore.QAbstractTableModel):
    def __init__(self, parent = None, metadata = None, hendler = None, elementi = []):
        super().__init__(parent)
        self.elementi = elementi
        self.metadata = metadata
        self.hendler = hendler

    def get_element(self, index):
        return self.elementi[index.row()]
    
    def rowCount(self, index):
        return len(self.elementi)

    def columnCount(self, index):
        return len(self.metadata['columns'])

    def data(self, index, role = QtCore.Qt.DisplayRole):
        element = self.get_element(index)

        if role == QtCore.Qt.DisplayRole:
            prom = self.metadata['columns'][index.column()]
            #print(prom)
            #print(element)
            #return element[self.metadata['columns'][index.column()]]
            return element[prom]
        return None

    def headerData(self, section, orientation, role = QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.metadata['columns'][section]
        return None

    def setData(self, index, value, role = QtCore.Qt.EditRole):
        element = self.get_element(index)

        if value == " ":
            return False
        if role == QtCore.Qt.EditRole:
            if self.metadata['type'] == "database":
                #kolona = self.metadata['columns'][index.column()]
                #kljuc = self.metadata['key']
                #kljuc_vrednost = element[kljuc]
                #kljuc_vrednost = element[self.metadata["columns"]]["key"]
                #print(kljuc)
                #print(kljuc_vrednost)
                
                element[self.metadata['columns'][index.column()]] = value 
                #print(element)

                prom = tuple(element)

                lista = []
                for i in prom:
                    trenutni = element[i]
                    lista.append(trenutni)
                objekat = tuple(lista)
                self.hendler.edit(objekat)
                #print(kolona)
                #print(value)
                #print(q)
                #print(prom)
            else:
                element[self.metadata['columns'][index.column()]] = value
                #print("datoteka je")
            #setattr(element, self.metadata['columns'][index.column()], value)
            #promenjeno zbog rada bez fiksnih klasa
            #print(element[self.metadata['columns'][index.column()]])
            return True

        return False

    def nadji_key_kolonu(self):
        for kolona in range(len(self.metadata['columns'])):
            if self.metadata['key'] == self.metadata['columns'][kolona]:
                return kolona

    def flags(self, index):
        if index.column() == self.nadji_key_kolonu():
            return super().flags(index) | QtCore.Qt.ItemIsSelectable
        else:
            return super().flags(index) | QtCore.Qt.ItemIsEditable
