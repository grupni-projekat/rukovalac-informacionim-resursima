from datoteke.sequential_data_handler import DataHandler
import json
import pickle

class SequentialFileHandler(DataHandler):
    def __init__(self, filepath, meta_filepath):
        super().__init__()
        self.filepath = filepath
        self.meta_filepath = meta_filepath
        self.data = []
        self.metadata = {}
        self.load_data()
    
    def load_data(self):
        #učitavanje podataka
        with open((self.filepath), 'rb') as dfile:
            self.data = pickle.load(dfile) #koristimo pickle za deserijalizaciju podataka

        #učitavanje metapodataka
        with open(self.meta_filepath) as meta_file:
            self.metadata = json.load(meta_file)  

    def binary_search(self, id):
        top = len(self.data) - 1
        bottom = 0

        while bottom <= top: 
            middle = (top+bottom)//2; 

            if self.data[middle][self.metadata['key']] == id: 
                return middle
            elif self.data[middle][self.metadata['key']] < id: 
                bottom = mid + 1
            else:
                top = mid - 1
        return None


    def get_one(self, id):
        top = len(self.data) - 1
        bottom = 0

        while bottom <= top: 
            middle = (top+bottom)//2; 

            if self.data[middle][self.metadata['key']] == id: 
                return self.data[middle]
            elif self.data[middle][self.metadata['key']] < id: 
                bottom = middle + 1
            else:
                top = middle - 1
        return None
    
    def get_all(self):
        return self.data

    def insert(self, id, obj):
        top_item = len(self.data) - 1
        bottom_item = 0
        najveci = True
        pronadjen = False

        while bottom_item <= top_item:
            middle = (top_item + bottom_item)//2

            if self.data[middle][self.metadata['key']] == id:
                print("Element postoji sa zadatim kljucem!!!")
                pronadjen = True
                break
            elif self.data[middle][self.metadata['key']] > id:
                self.data.insert(middle, obj)
                najveci = False
                break
            else:
                bottom_item = middle + 1

        if najveci == False and pronadjen == False:
            self.data.insert(middle + 1, obj)
        
        if pronadjen == False:
            with open(self.filepath, 'wb') as f:
                pickle.dump(self.data, f)

    def delete_one(self, id):
        top = len(self.data) - 1
        bottom = 0

        while top <= bottom:
             
            middle = (top + bottom)//2

            if self.data[middle][self.metadata['key']] == id:
                self.data.remove(self.data[middle])
            
            elif self.data[middle][self.metadata['key']] < id: 
                bottom = mid + 1
            else:
                top = mid - 1

        
    def edit(self, id, value):
        top = len(self.data) - 1
        bottom = 0

        while top <= bottom:
            middle = (top + bottom)//2

            if self.data[middle][self.metadata['key']] == id:
                self.data.append(value)
                with open(self.filepath, 'wb') as data:
                    pickle.dump(self.data, data)
            elif self.data[middle][self.metadata['key']] < id: 
                bottom = mid + 1
            else:
                top = mid - 1

    def save(self):
        with open(self.filepath, 'wb') as data:
            pickle.dump(self.data, data)
