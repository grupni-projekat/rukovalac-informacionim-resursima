from datoteke.data_handler import DataHandler
import pickle
import json

class SerialFileHandler(DataHandler):
    def __init__(self, filepath, meta_filepath):
        super().__init__()
        self.filepath = filepath
        self.meta_filepath = meta_filepath
        self.data = []
        self.metadata = {}
        self.load_data()

    def load_data(self):
        try:
            with open((self.filepath), 'rb') as dfile:
                self.data = pickle.load(dfile)
        except EOFError as error:
            print(error)
            self.data = []
        except Exception as greska:
            print(greska)
        with open((self.meta_filepath)) as meta_file:
            self.metadata = json.load(meta_file)
    
    def get_one(self, id):
        for d in self.data:
            if getattr(d, (self.metadata["key"])) == id:
                return d
        return None
    
    def get_all(self):
        return self.data

    def insert(self, obj):
        #print("insertovao sam u datoteku")
        self.data.append(obj)
        with open((self.filepath), 'wb') as dfile:
            pickle.dump(self.data, dfile)

    def delete_one(self, id):
        for d in self.data:
            if d[self.metadata['key']] == id:
                self.data.remove(d)
        with open(self.filepath, 'wb') as new_data:
            pickle.dump(self.data, new_data)

    def edit(self, id, value):
        pronadjen = False
        index = 0
        for i in self.data:
            if getattr(i, (self.metadata["key"])) == id:
                self.data[index] = value
                pronadjen = True
            else:
                index += 1
        if pronadjen == False:
            print("Ne postoji objekat u listi sa unetim id-em!")
    
    def insert_many(self, objects):
        for obj in self.objects:
            self.data.append(objects)

        with open((self.filepath), 'wb') as new_data:
            self.data = pickle.dump(self.data, new_data)

    def print_all(self):
        all_data = self.get_all()
        print(all_data)

    def save(self):
        with open(self.filepath, 'wb') as data:
            pickle.dump(self.data, data)

