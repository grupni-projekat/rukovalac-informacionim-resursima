from datoteke.data_handler import DataHandler
import mysql.connector
from mysql.connector import errorcode
import json
import pickle

def dobavi_konekciju(korisnik, lozinka, host, baza):
    cnx = mysql.connector.connect(user = korisnik, password = lozinka, host = host, database = baza)
    return cnx

class SqlHandler(DataHandler):
    def __init__(self, tabela, kljuc, metapath, brojac):
        super().__init__()
        try:
            self.tabela = tabela
            self.kljuc = kljuc
            self.metapath = metapath
            self.metadata = {}
            self.redni_broj = brojac

            with open(self.metapath, 'rb') as data:
                self.metadata = json.load(data)

            self.cnx = dobavi_konekciju(self.metadata['user'], self.metadata['password'], self.metadata['host'], self.metadata['database'])
            self.cursor = self.cnx.cursor()
            self.data = self.load_data()
            self.kolone = self.cursor.column_names

            self.cnx.commit()
                
        except mysql.connector.Error as error:
            if error.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Niste dobro uneli korisnicko ime ili lozinku za pristup bazi!")
            elif error.errno == errorcode.ER_BAD_DB_ERROR:
                print("Baza ne postoji!")
            else:
                print(error)
            self.cnx.close()
    
    def load_data(self):
        return self.get_all()

    def get_all(self):
        try:
            procedura = self.metadata['tables'][self.redni_broj]['read_procedure']
            self.cursor.callproc(procedura)
            self.cnx.commit()
            #rezultat = None
            for i in self.cursor.stored_results():
                j = i.fetchall()
                #rezultat = j
            
            #print(j)
            
            #rezultat = stored_result.fetchall()
            #print(rezultat)
            #print(self.metadata['columns'])
            self.metadata['key'] = self.kljuc
            self.metadata['columns'] = self.metadata['tables'][self.redni_broj]['columns']
            lista = []
            for i in j:
                #print(i)
                novi_u_bazi = {}
                brojac = 0
                for j in i:
                    novi_u_bazi[self.metadata['tables'][self.redni_broj]['columns'][brojac]] = str(j)
                    brojac += 1
                lista.append(novi_u_bazi)
            return lista
            
        except mysql.connector.Error as error:
            print(error)
            self.cnx.close()

    def get_one(self, obj):
        try:
            procedura = self.metadata['tables'][self.redni_broj]['search_procedure']
            self.cursor.callproc(procedura, (obj))
            self.cnx.commit()
            for i in self.cursor.stored_results():
                rez = i.fetchall()
            #return rez
            #print(rez)
            lista = []
            for i in rez:
                #print(i)
                novi_u_bazi = {}
                brojac = 0
                for j in i:
                    novi_u_bazi[self.metadata['tables'][self.redni_broj]['columns'][brojac]] = str(j)
                    brojac += 1
                lista.append(novi_u_bazi)
            return lista
        except mysql.connector.Error as error:
            print(error)
            self.cnx.rollback()
            self.cnx.close()

    def delete_one(self, id):
        try:
            procedura = self.metadata['tables'][self.redni_broj]['delete_procedure']
            #print(procedura)
            self.cursor.callproc(procedura, (id,))
            self.cnx.commit()
        except mysql.connector.Error as error:
            print(error)
            self.cnx.rollback()
            self.cnx.close()
    
    def edit(self, obj):
        try:
            procedura = self.metadata['tables'][self.redni_broj]['update_procedure']
            self.cursor.callproc(procedura, (obj))
            self.cnx.commit()
        except mysql.connector.Error as error:
            print(error)
            self.cnx.rollback()
            self.cnx.close()

    def insert(self, obj):
        try:
            procedura = self.metadata['tables'][self.redni_broj]['insert_procedure']
            print(procedura)
            self.cursor.callproc(procedura, (obj))
            self.cnx.commit()
        except mysql.connector.Error as error:
            print(error)
            self.cnx.rollback()
            self.cnx.close()

    def insert_many(self, obj):
        for i in obj:
            self.insert(i)

    def save(self, obj):
        pass