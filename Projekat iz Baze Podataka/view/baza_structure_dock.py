from PySide2 import QtGui, QtCore, QtWidgets
import json
from datoteke.sql_handler import SqlHandler

class DataBaseStructureDock(QtWidgets.QDockWidget):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.main_window = parent
        self.db_system_model = QtWidgets.QFileSystemModel()
        self.metadata = None
        
        self.tree = QtWidgets.QTreeView()
        self.tree.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.db_system_model.setRootPath(QtCore.QDir.currentPath())

        self.tree.setModel(self.db_system_model)
        self.tree.setRootIndex(self.db_system_model.index(QtCore.QDir.currentPath() + "/baza"))
        self.setWidget(self.tree)
        self.tree.clicked.connect(self.tree_view_select)

    def tree_view_select(self, index):
        self.path = self.db_system_model.filePath(index)
        lista_tabela = []
        print(self.path)

        with open(self.path, "rb") as data:
            self.metadata = json.load(data)

        self.standard_item_model = QtGui.QStandardItemModel()
        self.standard_item_model.setHorizontalHeaderLabels([""])
        standard_item = QtGui.QStandardItem(self.metadata['database'])

        for i in self.metadata['tables']:
            naziv_tabele = i['table_name']
            lista_tabela.append(naziv_tabele)

        for tabela in lista_tabela:
            tabela_item = QtGui.QStandardItem(tabela)
            standard_item.appendRow(tabela_item)
        self.standard_item_model.appendRow(standard_item)
        
        self.tree.setModel(self.standard_item_model)
        self.tree.clicked.disconnect(self.tree_view_select)
        self.tree.clicked.connect(self.selektovana_tabela)

    def selektovana_tabela(self, index):
        tabela = None
        brojac = 0
        for i in self.metadata['tables']:
            if index.data() == i['table_name']:
                self.metadata['columns'] = i['columns']
                tabela = SqlHandler(i['table_name'], i['key'], self.path, brojac)
            brojac += 1
        self.main_window.load_bazu(tabela)
        
        

    