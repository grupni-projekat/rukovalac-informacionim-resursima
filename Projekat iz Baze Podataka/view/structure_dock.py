from PySide2 import QtWidgets, QtGui, QtCore

class StructureDock(QtWidgets.QDockWidget):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.main_window = parent
        self.file_system_model = QtWidgets.QFileSystemModel()

        self.file_system_model.setRootPath(QtCore.QDir.currentPath())

        self.tree_view = QtWidgets.QTreeView()
        self.tree_view.setModel(self.file_system_model)
        self.tree_view.setRootIndex(self.file_system_model.index(QtCore.QDir.currentPath()))

        self.setWidget(self.tree_view)
        self.tree_view.clicked.connect(self.tree_view_select)

    
    def tree_view_select(self, index):
        self.path = self.file_system_model.filePath(index)
        print(self.path)
        self.main_window.load_file(self.path)