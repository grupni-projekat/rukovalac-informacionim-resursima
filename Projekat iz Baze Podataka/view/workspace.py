from PySide2 import QtWidgets, QtGui, QtCore
from modeli.universal_model import UniversalModel
import podaci
import pickle
import os
import json


class WorkspaceWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_layout = QtWidgets.QVBoxLayout()
        self.tab_widget = None
        self.trenutni_data = None
        self.sub_trenutni_data = None
        self.selektovan_element = 0
        #self.search_element = {}
        self.universal_insert_element = {}
        self.svi_line_edit = {}
        self.create_tab_widget()

        self.table1 = QtWidgets.QTableView(self.tab_widget)
        self.table1.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.table1.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.table1.setModel(None)
        self.table1.clicked.connect(self.show_subtable)

        self.subtable1 = QtWidgets.QTableView(self.tab_widget)
        self.subtable1.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.subtable1.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        self.tab_widget.addTab(self.subtable1, QtGui.QIcon("icons8-edit-file-64.png"), "Povezana tabela")
        self.main_layout.addWidget(self.table1)
        self.main_layout.addWidget(self.tab_widget)
        self.setLayout(self.main_layout)

    #ucitavanje podataka na table_view
    def load_table(self, filehandler):
        univerzalni_model = UniversalModel(self, filehandler.metadata, filehandler)
        univerzalni_model.elementi = filehandler.get_all()
        #print(filehandler.get_all())
        self.table1_elementi = univerzalni_model.elementi
        self.table1.setModel(univerzalni_model)
        self.trenutni_data = filehandler

    def show_subtable(self):
        self.tab_widget.addTab(self.subtable1, QtGui.QIcon("icons8-edit-file-64.png"), "Povezana tabela")

    def load_sql_table(self, sql_handler):
        self.univeralni_model = UniversalModel(self, sql_handler.metadata, sql_handler)
        self.univeralni_model.elementi = sql_handler.get_all()
        #print(sql_handler.get_all())
        self.table1_elementi = self.univeralni_model.elementi
        self.table1.setModel(self.univeralni_model)
        self.trenutni_data = sql_handler

    def load_subtable(self, subtable_file_handler):
        subtable_univerzalni_model = UniversalModel(self, subtable_file_handler.metadata)
        subtable_univerzalni_model.elementi = subtable_file_handler.get_all()
        #self.subtable_elementi = subtable_univerzalni_model.elementi
        self.subtable1.setModel(subtable_univerzalni_model)
        self.subtable_trenutni_data = subtable_file_handler

    #radi refresh tabele nakon insert akcije i delete akcije
    def refresh_main_table(self, filehandler):
        refresh_model = UniversalModel(self, self.trenutni_data.metadata)
        refresh_model.elementi = self.trenutni_data.get_all()
        self.table1.setModel(refresh_model)
        self.trenutni_data = filehandler

    def create_tab_widget(self):
        self.tab_widget = QtWidgets.QTabWidget(self)
        self.tab_widget.setTabsClosable(True)
        self.tab_widget.tabCloseRequested.connect(self.delete_tab)

    def delete_tab(self, index):
        self.tab_widget.removeTab(index)

    def delete(self):
        index = self.table1.currentIndex()
        red = index.row()
        if not index.isValid():
            greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Greska", "Niste oznacili ni jedan red iz tabele!")
            greska.exec_()
        else:
            element = self.table1.model().elementi[red]
            #print(index)
            #print(element)
            #self.trenutni_data.delete_one(getattr(element, self.trenutni_data.metadata['key']))
            prom = element[self.trenutni_data.metadata['key']]
            self.trenutni_data.delete_one(prom)
            self.load_table(self.trenutni_data)

    #def delete_from_database(self):
        #index = self.table1.currentIndex()
        #red = index.row()
        #if not index.isValid():
            #greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Greska", "Niste oznacili ni jedan red iz tabele!")
            #greska.exec_()
        #else:
            #element = self.table1.model().elementi[red]
            #prom = element[self.trenutni_data.metadata['key']]
            #print(prom)
            #self.trenutni_data.delete_one(prom)
            #self.load_sql_table(self.trenutni_data)

    def save(self):
        self.trenutni_data.save()

    def insert(self):
        #self.svi_line_edit = []
        self.insert_dialog = QtWidgets.QDialog()
        self.insert_dialog.setWindowTitle("Unos novog elementa u tabelu")
        self.vertical_layout = QtWidgets.QVBoxLayout()
        self.dodaj = QtWidgets.QPushButton("Dodaj novi element")
        self.universal_insert_element = {}
        
        brojac = 0
        for kolona in self.trenutni_data.metadata['columns']:
            horizontal_layout = QtWidgets.QHBoxLayout()
            tren_kolona = self.trenutni_data.metadata['columns'][brojac].replace("_", " ")
            if tren_kolona.islower():
                tren_kolona = tren_kolona.capitalize()
            #print(tren_kolona)
            labela = QtWidgets.QLabel(tren_kolona + ":")
            unos = QtWidgets.QLineEdit()
            self.universal_insert_element[kolona] = unos
            #self.svi_line_edit.append(unos)
            #print(self.svi_line_edit)
            horizontal_layout.addWidget(labela)
            horizontal_layout.addWidget(unos)

            brojac += 1
            self.vertical_layout.addLayout(horizontal_layout)
        
        

        self.dodaj.clicked.connect(self.dodaj_red)
        self.vertical_layout.addWidget(self.dodaj)
        self.insert_dialog.setLayout(self.vertical_layout)
        self.insert_dialog.setWindowTitle("Insert u datoteku")
        self.insert_dialog.show()

    def dodaj_red(self):
        #print("dugme je stisnuto!!!")
        #tip_elementa = type(self.table1.model().elementi[0])
        #trenutna_klasa = tip_elementa()
        table_model = self.table1.model()
        #trenutni_element = {}
        tren = {}
        tren_lista = []

        pronadjen = False
        for i in self.trenutni_data.get_all():
            #print(i)
            trenutni_kljuc = i[self.trenutni_data.metadata['key']]
            #print(trenutni_kljuc)
            #print(self.universal_insert_element)
            if trenutni_kljuc == self.universal_insert_element[self.trenutni_data.metadata['key']].text():
                pronadjen = True
                #print(trenutni_kljuc)
        if pronadjen:
            greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Greska", "Kljuc koji zelite da unesete vec postoji!")
            greska.exec()
        else:
            for kolona in self.trenutni_data.metadata['columns']:
                if self.trenutni_data.metadata['type'] == "database":
                    tren_lista.append(self.universal_insert_element[kolona].text())
                    print(tren_lista)
                    prom = tuple(tren_lista)
                elif self.trenutni_data.metadata['type'] == "serijska" or self.trenutni_data.metadata["sekvencijalna"]:
                    tren[kolona] = self.universal_insert_element[kolona].text()
                    prom = tren
            self.trenutni_data.insert(prom)
            self.table1.setModel(table_model)
            self.refresh_main_table(self.trenutni_data)

        #print(prom)
                #table_model.elementi.append(prom)
                #self.trenutni_data.insert(prom)
        #print(tren_lista)
        #x = 0
        #for kolona in self.trenutni_data.metadata['columns']:
            #print(kolona)
            #print(x)
            #setattr(trenutna_klasa, kolona, self.svi_line_edit[x].text())
            #trenutni_element[kolona] = self.svi_line_edit[x].text()
            #print(trenutni_element)
            #x += 1
        
        
        #print(trenutna_klasa)
        #self.trenutni_data.insert(trenutna_klasa)
        #else:

    def jedan_gore(self):
        if self.selektovan_element == 0:
            self.table1.selectRow(self.selektovan_element)
        else:
            self.selektovan_element -= 1
            self.table1.selectRow(self.selektovan_element)
    
    def jedan_dole(self):
        if self.selektovan_element == (len(self.table1_elementi)-1):
            self.table1.selectRow(len(self.table1_elementi)-1)
        else:    
            self.selektovan_element += 1
            self.table1.selectRow(self.selektovan_element)

    def prvi(self):
        self.selektovan_element = 0
        self.table1.selectRow(self.selektovan_element)

    def poslednji(self):
        self.selektovan_element = (len(self.table1_elementi) - 1)
        self.table1.selectRow(self.selektovan_element)

    def delete_file(self, path):
        os.remove(path)
    
    def kreiranje_novog_metadata(self):
        q = 1
        while q == 1:
            self.user, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "User: ", QtWidgets.QLineEdit.Normal, "")   
            if ok_pressed and self.user != "":
                #print(self.user)
                break
            else:
                greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!", QtWidgets.QMessageBox.Close)
                greska.exec_()
                q = 1
        z = 1
        while z == 1:
            self.password, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Password: ", QtWidgets.QLineEdit.Normal, "")   
            if ok_pressed:
                break
        w = 1
        while w == 1:
            self.host, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Host: ", QtWidgets.QLineEdit.Normal, "")
            if ok_pressed and self.host != "":
                #print(self.user, self.password, self.host)
                break
            else:
                greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                greska.exec_()
                w = 1
        e = 1
        while e == 1:
            self.database, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Ime baze: ", QtWidgets.QLineEdit.Normal, "")
            if ok_pressed and self.database != "":
                break
            else:
                greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                greska.exec_()
                e = 1

        r = 1
        while r == 1:
            self.broj_tabela, ok_pressed = QtWidgets.QInputDialog.getInt(self, "Kreiranje novog metadata", "Broj tabela u Vasoj bazi: ", 0, 0, 1000, 1)
            if ok_pressed and self.broj_tabela != "":
                #print(self.user, self.password, self.host, self.database, self.broj_tabela)
                break
            else:
                greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                greska.exec_()
                r = 1

        broj_dialoga = 0
        self.tabela = []
        while broj_dialoga < self.broj_tabela:
            a = 1
            while a == 1:
                self.table_name, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Ime tabele: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.table_name != "":
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    a = 1
            b = 1
            while b == 1:
                self.key, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Key tabele: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.key != "":
                    #self.tabela.append()
                    #broj_dialoga += 1
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    b = 1
            
            #print(self.tabela)
            
            c = 1
            while c == 1:
                self.broj_kolona, ok_pressed = QtWidgets.QInputDialog.getInt(self, "Kreiranje novog metadata", "Broj kolona u vasoj tabeli: ", 0, 0, 1000, 1)
                if ok_pressed and self.broj_kolona != "":
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    c = 1
            
            broj_kolona = 0
            kolone = []
            while broj_kolona < self.broj_kolona:
                x = 1
                while x == 1:
                    self.ime_kolone, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Ime kolone: ", QtWidgets.QLineEdit.Normal, "")
                    if ok_pressed and self.ime_kolone != "":
                        kolone.append(self.ime_kolone)
                        break
                    else:
                        greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                        greska.exec_()
                        x = 1
                #print(self.tabela)
                broj_kolona += 1
            v = 1
            while v == 1:
                self.insert_procedure, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Naziv insert procedure: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.insert_procedure != "":
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    v = 1
            b = 1
            while b == 1:
                self.delete_procedure, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Naziv delete procedure: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.delete_procedure != "":
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    b = 1
            n = 1
            while n == 1:
                self.update_procedure, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Naziv update procedure: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.update_procedure != "":
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    n = 1
            m = 1
            while m == 1:
                self.read_procedure, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Naziv read procedure: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.read_procedure != "":
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    m = 1
            l = 1
            while l == 1:
                self.search_procedure, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Naziv search procedure: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.search_procedure != "":
                    self.tabela.append({"table_name":self.table_name, "key":self.key, "columns":kolone, "insert_procedure": self.insert_procedure, "delete_procedure": self.delete_procedure, "update_procedure":self.update_procedure, "read_procedure": self.read_procedure, "search_procedure": self.search_procedure})
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    l = 1
            print(self.tabela)
            broj_dialoga += 1
        self.upisi_novi_metadata()


        #print(broj_dialoga)
    
    def upisi_novi_metadata(self):
        metadata = {}
        metadata["type"] = "database"
        metadata["columns"] = []
        metadata["key"] = ""
        metadata["user"] = self.user
        metadata["password"] = self.password
        metadata["host"] = self.host
        metadata["database"] = self.database
        metadata["tables"] = self.tabela

        with open("baza/" + self.database + "_metadata.json", 'w') as data:
            json.dump(metadata, data)

    def kreiranje_novog_fajla(self):
        q = 1
        while q == 1:
            self.broj_kolona, ok_pressed = QtWidgets.QInputDialog.getInt(self, "Kreiranje novog metadata", "Broj kolona: ", 0, 0, 1000, 1)
            if ok_pressed and self.broj_kolona != "":
                break
            else:
                greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                greska.exec_()
                q = 1

        broj_kolona = 0
        self.columns = []
        while broj_kolona < self.broj_kolona:
            w = 1
            while w == 1:
                self.naziv_kolone, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Ime kolone: ", QtWidgets.QLineEdit.Normal, "")
                if ok_pressed and self.naziv_kolone != "":
                    break
                else:
                    greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                    greska.exec_()
                    w = 1
            #print(self.naziv_kolone)
            self.columns.append(self.naziv_kolone)
            broj_kolona += 1
            print(self.columns)
        e = 1
        while e == 1:
            self.ime_fajla, ok_pressed = QtWidgets.QInputDialog.getText(self, "Kreiranje novog metadata", "Ime fajla: ", QtWidgets.QLineEdit.Normal, "")
            if ok_pressed and self.ime_fajla != "":
                self.dialog = QtWidgets.QDialog(self)

                self.key_label = QtWidgets.QLabel("Izaberite key kolonu: ")
                self.key_combo_box = QtWidgets.QComboBox(self)

                for i in self.columns:
                    self.key_combo_box.addItem(i)

                self.type = QtWidgets.QLabel("Izaberite tip datoteke: ")
                self.type_combo_box = QtWidgets.QComboBox(self)
                self.type_combo_box.addItem("serijska")
                self.type_combo_box.addItem("sekvencijalna")

                self.button = QtWidgets.QPushButton("Ok")
                self.button.clicked.connect(self.upisi_novi_metadata_fajl)

                layout = QtWidgets.QVBoxLayout()
                layout.addWidget(self.key_label)
                layout.addWidget(self.key_combo_box)
                layout.addWidget(self.type)
                layout.addWidget(self.type_combo_box)
                layout.addWidget(self.button)

                self.dialog.setWindowTitle("Metadata")
                self.dialog.setLayout(layout)
                self.dialog.show()
                break
            else:
                greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Ovo polje ne sme biti prazno!")
                greska.exec_()
                e = 1

    def upisi_novi_metadata_fajl(self):
        metadata = {}
        metadata["columns"] = self.columns
        metadata["key"] = self.key_combo_box.currentText()
        metadata["type"] = self.type_combo_box.currentText()
        metadata["linked_files"] = []

        f = open("podaci/" + self.ime_fajla + "_data", "w")
        f.close()

        with open("podaci/" + self.ime_fajla + "_metadata.json", "w") as data:
            json.dump(metadata, data)

        self.dialog.accept()

    def search(self):
        self.dialog = QtWidgets.QDialog()
        layout = QtWidgets.QVBoxLayout()
        #self.labela = QtWidgets.QLabel("Unesite tekst za pretragu: ")
        self.svi_line_edit = []

        brojac = 0
        for kolona in self.trenutni_data.metadata['columns']:
            h_layout = QtWidgets.QHBoxLayout()
            tren_kolona = self.trenutni_data.metadata["columns"][brojac].replace("_", " ")
            if tren_kolona.islower():
                tren_kolona = tren_kolona.capitalize()
            self.labela = QtWidgets.QLabel(tren_kolona + ": ")
            self.search_line_edit = QtWidgets.QLineEdit()
            self.svi_line_edit.append(self.search_line_edit)
            h_layout.addWidget(self.labela)
            h_layout.addWidget(self.search_line_edit)
            layout.addLayout(h_layout)
            brojac += 1
        self.button = QtWidgets.QPushButton("Pretraga...")

        layout.addWidget(self.button)
        self.button.clicked.connect(self.pretrazi_elemente)
        self.dialog.setWindowTitle("Pretraga")
        self.dialog.setLayout(layout)
        self.dialog.show()


    def pretrazi_elemente(self):
        table_model = self.table1.model()
        pronadjen = []
        brojac = 0
        brojac_drugi = 0

        #search za baze, radi sve
        universal_model = UniversalModel(self, self.trenutni_data.metadata, self.trenutni_data)
        if self.trenutni_data.metadata['type'] == "database":
            for kolona in self.trenutni_data.metadata['columns']:
                pronadjen.append(self.svi_line_edit[brojac].text())
                brojac += 1
            #print(pronadjen)
            prom = tuple(pronadjen)
            #print(prom)
            
            universal_model.elementi = self.trenutni_data.get_one(prom)

        #search za datoteke baguje, radi samo za trazenje jednog elementa, ne moze da trazi vise po vise kriterijuma
        else:
            tren = {}
            for kolona in self.trenutni_data.metadata['columns']:
                for i in self.table1_elementi:
                    tren = self.svi_line_edit[brojac_drugi].text()
                    if i[kolona] == tren:
                        pronadjen.append(i)
                brojac_drugi += 1
            print(pronadjen)

            universal_model.elementi = pronadjen
        if pronadjen != []:
            self.table1_elementi = universal_model.elementi
            self.trenutni_data = self.trenutni_data
            self.table1.setModel(universal_model)
        else:
            greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical, "Greska", "Uneli ste podatke koji ne postoje u bazi!")
            greska.exec_()