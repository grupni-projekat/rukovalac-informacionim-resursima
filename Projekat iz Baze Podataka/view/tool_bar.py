from PySide2 import QtWidgets, QtGui, QtCore

class ToolBar(QtWidgets.QToolBar):
    def __init__(self, parent):
        super().__init__()
        self.main_window = parent

        metadata_za_bazu = QtWidgets.QAction(QtGui.QIcon("ikonice/novi_metadata_za_bazu.png"), "New metadata for database", self)
        metadata_za_bazu.setStatusTip("New metadata for database")
        metadata_za_bazu.triggered.connect(self.main_window.novi_metadata)

        brisanje_fajla = QtWidgets.QAction(QtGui.QIcon("ikonice/brisanje_fajla.ico"), "Delete file", self)
        brisanje_fajla.setStatusTip("Delete file")
        brisanje_fajla.triggered.connect(self.main_window.brisanje_fajla_method)

        dodavanje_fajla = QtWidgets.QAction(QtGui.QIcon("ikonice/novi_fajl.jpg"), "Insert file", self)
        dodavanje_fajla.setStatusTip("Add new file")
        dodavanje_fajla.triggered.connect(self.main_window.dodavanje_fajla_method)

        save_action = QtWidgets.QAction(QtGui.QIcon("ikonice/save.png"),"Save", self)
        save_action.setStatusTip("Save")
        save_action.triggered.connect(self.main_window.save_method)

        delete_action = QtWidgets.QAction(QtGui.QIcon("ikonice/delete_row.png"), "Delete", self)
        delete_action.setStatusTip("Delete")
        delete_action.triggered.connect(self.main_window.delete_method)

        insert_action = QtWidgets.QAction(QtGui.QIcon("ikonice/insert_row.png"), "Insert", self)
        insert_action.setStatusTip("Insert")
        insert_action.triggered.connect(self.main_window.insert_method)

        jedan_gore_action = QtWidgets.QAction(QtGui.QIcon("ikonice/jedan_gore.jpg"), "One up", self)
        jedan_gore_action.setStatusTip("One up")
        jedan_gore_action.triggered.connect(self.main_window.jedan_gore_method)

        jedan_dole_action = QtWidgets.QAction(QtGui.QIcon("ikonice/jedan_dole.jpg"), "One down", self)
        jedan_dole_action.setStatusTip("One down")
        jedan_dole_action.triggered.connect(self.main_window.jedan_dole_method)

        prvi_action = QtWidgets.QAction(QtGui.QIcon("ikonice/prvi.png"), "First", self)
        prvi_action.setStatusTip("First")
        prvi_action.triggered.connect(self.main_window.prvi_method)

        poslednji_action = QtWidgets.QAction(QtGui.QIcon("ikonice/poslednji.png"), "Last", self)
        poslednji_action.setStatusTip("Last")
        poslednji_action.triggered.connect(self.main_window.poslednji_method)

        #insert_action_baza = QtWidgets.QAction(QtGui.QIcon("ikonice/dodaj_u_bazu.png"), "Insert in database", self)
        #insert_action_baza.setStatusTip("Insert in database")
        #insert_action_baza.triggered.connect(self.main_window.insert_method_baza)

        #delete_action_baza = QtWidgets.QAction(QtGui.QIcon("ikonice/obrisi_red_baza.png"), "Delete from database", self)
        #delete_action_baza.setStatusTip("Delete from database")
        #delete_action_baza.triggered.connect(self.main_window.delete_method_baza)

        search_action = QtWidgets.QAction(QtGui.QIcon("ikonice/search.png"), "Search", self)
        search_action.setStatusTip("Search")
        search_action.triggered.connect(self.main_window.search_method)

        self.addAction(metadata_za_bazu)
        self.addAction(brisanje_fajla)
        self.addAction(dodavanje_fajla)
        self.addAction(save_action)
        self.addAction(delete_action)
        self.addAction(insert_action)
        #self.addAction(delete_action_baza)
        #self.addAction(insert_action_baza)
        self.addAction(jedan_gore_action)
        self.addAction(jedan_dole_action)
        self.addAction(prvi_action)
        self.addAction(poslednji_action)
        self.addAction(search_action)