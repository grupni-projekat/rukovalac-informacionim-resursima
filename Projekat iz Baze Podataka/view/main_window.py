from PySide2 import QtWidgets, QtCore, QtGui
from view.structure_dock import StructureDock
from datoteke.serial_file_handler import SerialFileHandler
from datoteke.sequential_file_handler import SequentialFileHandler
from view.workspace import WorkspaceWidget
from view.baza_structure_dock import DataBaseStructureDock
from view.tool_bar import ToolBar
import json
import pickle
import sys
import pathlib


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.resize(1200, 700)
        self.setWindowTitle("Editor generickih podataka")
        self.setWindowIcon(QtGui.QIcon("ikonice/icons8-edit-file-64.png"))

        self.structure_dock = StructureDock("Structure dock", self)
        self.data_base_structure_dock = DataBaseStructureDock("Data Base Structure Dock", self)
        self.menu_bar = QtWidgets.QMenuBar(self)

        file_menu = QtWidgets.QMenu("File", self)

        new_file_action = QtWidgets.QAction("New File", self)
        new_file_action.setStatusTip("Create new file")
        new_file_action.setShortcut(None)

        quit_app_action = QtWidgets.QAction("Quit", self)
        quit_app_action.setStatusTip("Quit application")
        quit_app_action.setShortcut(None)
        quit_app_action.triggered.connect(self.quit_method)

        file_menu.addAction(new_file_action)
        file_menu.addAction(quit_app_action)

        edit_menu = QtWidgets.QMenu("Edit", self)

        help_menu = QtWidgets.QMenu("Help", self)

        view_menu = QtWidgets.QMenu("View", self)
        toggle_structure_dock_action = self.structure_dock.toggleViewAction()
        view_menu.addAction(toggle_structure_dock_action)

        self.menu_bar.addMenu(file_menu)
        self.menu_bar.addMenu(edit_menu)
        self.menu_bar.addMenu(view_menu)
        self.menu_bar.addMenu(help_menu)

        self.tool_bar = ToolBar(self)

        self.central_widget = QtWidgets.QTabWidget(self)
        self.workspace = WorkspaceWidget(self)
        self.central_widget.addTab(self.workspace, QtGui.QIcon("ikonice/icons8-edit-file-64.png"), "Prikaz tabele")
        self.central_widget.setTabsClosable(True)

        self.status_bar = QtWidgets.QStatusBar(self)
        self.status_bar.showMessage("Status bar je prikazan!")

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.structure_dock)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.data_base_structure_dock)
        self.setCentralWidget(self.central_widget)
        

    def load_file(self, data_path):
        metadata_path = data_path.replace("_data", "_metadata.json")
        with open(metadata_path) as data:
            metadata = json.load(data)

        linked_files = metadata['linked_files']
        #print(linked_files)
        if len(linked_files) == 0:
            greska = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Obavestenje", "Ne postoji ni jedna povezana tabela za odabran fajl.")
            greska.exec_()
        else:
            for i in linked_files:
                #print(i)
                linked_path = data_path[:data_path.rindex("/")]
                linked_data_path = linked_path + "/" + i
                linked_metadata_path = linked_data_path.replace("_data", "_metadata.json")
                #print(linked_path)
                #print(linked_data_path)
                with open(linked_metadata_path) as data:
                    linked_metadata = json.load(data)
                if linked_metadata['type'] == 'serijska':
                    serijska = SerialFileHandler(linked_data_path, linked_metadata_path)
                    self.workspace.load_subtable(serijska)
                elif linked_metadata['type'] == 'sekvencijalna':
                    sekvencijalna = SequentialFileHandler(linked_data_path, linked_metadata_path)
                    self.workspace.load_subtable(sekvencijalna)
        if metadata['type'] == 'serijska':
            serijska = SerialFileHandler(data_path, metadata_path)
            self.workspace.load_table(serijska)
        elif metadata['type'] == 'sekvencijalna':
            sekvencijalna = SequentialFileHandler(data_path, metadata_path)
            self.workspace.load_table(sekvencijalna)

    def load_bazu(self, tabela):
        self.workspace.load_sql_table(tabela)

    #def insert_method_baza(self):
        #print("radi")
        #self.workspace.insert()

    #def delete_method_baza(self):
        #self.workspace.delete()

    def save_method(self):
        self.workspace.save()
        #print("sacuvan!")

    def quit_method(self):
        return sys.exit()

    def delete_method(self):
        self.workspace.delete()
        #print("obrisan")

    def insert_method(self):
        self.workspace.insert()

    def jedan_gore_method(self):
        #print("gore")
        self.workspace.jedan_gore()

    def jedan_dole_method(self):
        #print("dole")
        self.workspace.jedan_dole()

    def prvi_method(self):
        #print("prvi")
        self.workspace.prvi()

    def poslednji_method(self):
        #print("poslednji")
        self.workspace.poslednji()

    def novi_metadata(self):
        #print("metadata")
        self.workspace.kreiranje_novog_metadata()
    
    def brisanje_fajla_method(self):
        #print("brisanje")
        self.workspace.delete_file(self.structure_dock.path)

    def dodavanje_fajla_method(self):
        #print("dodavanje_fajla")
        self.workspace.kreiranje_novog_fajla()

    def search_method(self):
        self.workspace.search()
