import sys
from PySide2 import QtWidgets, QtGui, QtCore
from view.main_window import MainWindow
import pickle
import json


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())